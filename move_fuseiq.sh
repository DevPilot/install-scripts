#!/bin/bash

# site is first argument
site2=$1
site2=$site2

sshuser=ENTERAUSERNAME

echo "This should be run from one directory underneath all the project directories"

if [[ -d $site2 ]]; then
    echo "$site2 is a directory... continuing"
elif [[ -f $site2 ]]; then
    echo "$site2 is a file"
    exit 1
else
    echo "$site2 is not valid"
    exit 1
fi

mysqldump -uroot -proot $site2 > databases/`date +%Y%m%d`.$site2.sql



rsync -avz $site2/ $sshuser@54.201.98.159:/var/www/$site2/

ssh -t dsmythjr@54.201.98.159 "mkdir /var/www/$site2/databases"

rsync -avz databases/`date +%Y%m%d`.$site2.sql $sshuser@54.201.98.159:/var/www/$site2/databases/$site2.sql

ssh -t $sshuser@54.201.98.159 "sudo bash /var/www/enable.sh $site2"