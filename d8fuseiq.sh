#!/bin/bash

# set some bold and normal text properties
bold=$(tput bold)
normal=$(tput sgr0)

# set the local database information
localdbuser=root
localdbpw=root

# Give some options for the install profile and theme
echo "This should be run from one directory underneath all the project directories"
echo "Rememeber to change the database password when this is placed on any public facing server"

PS3='Which install combo would you like? '
options=("First and Cherry" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "First and Cherry")
            profilename=First
            profilegit="git@bitbucket.org:DevPilot/fuseiq-starter-first.git web/profiles/custom/fuseiq_starter_first"
            themename=Cherry
            themegit="git@bitbucket.org:DevPilot/fuse-iq-cherry-theme.git web/themes/custom/fuseiq_cherry"
            break
            ;;
        "Quit")
            exit 1
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

echo
echo "You chose profile/theme combo ${bold}$profilename${normal} and ${bold}$themename${normal}"
echo
#confirm to continue
PS3='Would you like to continue? '
options=("Yes" "No")
select opt in "${options[@]}"
do
    case $opt in
        "Yes")
            break
            ;;
        "No")
            exit 1
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

# Ask for the project name
echo

echo What would you like the project named? \(one word, no spaces\)

read sitename
echo
echo Your site will be named $sitename

echo ==============
echo
echo
# git clone $profilegit

composer create-project drupal-composer/drupal-project:8.x-dev $sitename --stability dev --no-interaction

# mysql vars
mysql_pass=$localdbpw
mysql_connection="-h localhost -p3306 -u $localdbuser -p$mysql_pass"

# setup database
echo
echo "Creating Password"
echo
weasel="weaselpw"
dbpass=$(openssl rand -base64 6)
echo "Creating Database"
echo
mysql $mysql_connection -e "create database $sitename";
mysql $mysql_connection -e "create user $sitename";
mysql $mysql_connection -e "grant all on $sitename.* to '$sitename'@'%' identified by '$weasel';"


cd $sitename
mkdir tmp
mkdir web/themes/custom
mkdir web/profiles/custom
mkdir web/modules/custom

git clone $themegit
git clone git@bitbucket.org:DevPilot/fuseiq.git web/modules/custom/fuseiq
git clone $profilegit
git clone git@bitbucket.org:DevPilot/fuseiq-mobile-menu.git web/modules/custom/fuseiq_mobile_menu
git clone git@bitbucket.org:DevPilot/fuseiq-generic-paragraph.git web/modules/custom/fuseiq_generic_prgph

composer require drupal/bootstrap
composer require drupal/paragraphs
composer require drupal/ctools
composer require drupal/field_group
echo ===========================
echo ===========================
echo ===========================
#echo $dbpass - Write this down!
userpass=$(openssl rand -base64 6)
drush site-install --db-su=root --db-su-pw=root --db-url=mysql://$sitename:$weasel@localhost:3306/$sitename --site-name=$sitename --locale="en" --account-name=FuseIQAdmin --account-pass=$userpass -y
echo ===========================
echo "Your user name is FuseIQAdmin and the Password is $userpass"
